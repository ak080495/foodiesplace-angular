import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
	
import { MaterialModule } from './material/material.module';
import { LayoutComponent } from './layout/layout.component';


import {FlexLayoutModule} from '@angular/flex-layout';
import { HomeComponent, BottomSheetOverviewExampleSheet } from './home/home.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent ,MenuExtend} from './menu/menu.component';


import { SliderModule } from 'angular-image-slider';

import { BootstrapcarouselComponent } from './bootstrapcarousel/bootstrapcarousel.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//slider

import { NgImageSliderModule } from 'ng-image-slider';



import { AboutComponent } from '../app/component/about/about.component';
import { BlogComponent } from '../app/component/blog/blog.component';
import { ContactComponent } from '../app/component/contact/contact.component';
import { GalleryComponent } from '../app/component/gallery/gallery.component';
import { GrdFilterPipe } from './grd-filter.pipe';
import { from } from 'rxjs';
import { ItemdetailComponent } from './itemdetail/itemdetail.component';
import { AdditemComponent } from './additem/additem.component';
import { AgmCoreModule } from '@agm/core';

import { FilterimagesPipe } from './filterimages.pipe';

import {MatDialogModule} from '@angular/material/dialog';
import { LoginComponent } from './registation/login/login.component';


import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { SignupComponent } from './registation/signup/signup.component';

import { HttpClientModule,HTTP_INTERCEPTORS ,HttpClientXsrfModule} from '@angular/common/http';
import { AlertComponent } from './registation/_components/alert/alert.component';
import { InternalServerComponent } from './error-pages/internal-server/internal-server.component';

import { ProfileComponent } from './registation/profile/profile.component';
import { ImageuploadComponent } from './imageupload/imageupload.component';


import { FileUploadModule } from 'ng2-file-upload';
import { ChangePasswordComponent } from './registation/change-password/change-password.component';
import { ForgetPasswordComponent } from './registation/forget-password/forget-password.component';


// import {FileInputAccessorModule} from "file-input-accessor";
import {CookieService} from 'ngx-cookie-service';
import { ReserconfirmComponent } from './reserconfirm/reserconfirm.component'


import {
  MdcButtonModule,
  MdcFabModule,
  MdcIconModule,
} from '@angular-mdc/web';
// import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

import {MatBottomSheetModule} from '@angular/material/bottom-sheet'


@NgModule({
  declarations: [
    BottomSheetOverviewExampleSheet,
    AppComponent,
    LayoutComponent,
    HomeComponent,
    HeaderComponent,
    SidenavListComponent,
    FooterComponent,
    MenuComponent,MenuExtend,
    BootstrapcarouselComponent,
    AboutComponent,
    BlogComponent,
    ContactComponent,
    GalleryComponent,
    GrdFilterPipe,
    ItemdetailComponent,
    AdditemComponent,
    FilterimagesPipe,
    LoginComponent,
    SignupComponent,
    AlertComponent,
    InternalServerComponent,
    ProfileComponent,
    ImageuploadComponent,
    ChangePasswordComponent,
    ForgetPasswordComponent,
    ReserconfirmComponent,
    
    
  ],
  imports: [
    // MatBottomSheet,
    // MatBottomSheetRef,
    MatBottomSheetModule,
    MdcButtonModule,
    MdcFabModule,
    MdcIconModule,
    FileUploadModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    SliderModule,
    // FileInputAccessorModule,
    NgbModule,
    NgImageSliderModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDcyUsJ2SfifbL0Y_jO_gnCmZ0nRBwuyuQ'
      /* apiKey is required, unless you are a 
      premium customer, in which case you can 
      use clientId 
      */

     
    }),
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
    }),


  ],
  exports:[BottomSheetOverviewExampleSheet],

  providers: [CookieService],
  bootstrap: [AppComponent],
  entryComponents: [ReserconfirmComponent,LoginComponent,SignupComponent,MenuExtend,ImageuploadComponent,ChangePasswordComponent,ForgetPasswordComponent,BottomSheetOverviewExampleSheet],
  
})
export class AppModule { }

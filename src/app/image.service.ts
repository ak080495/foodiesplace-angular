import { Injectable } from '@angular/core' 
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
  }) 
export class ImageService {    
    // private baseUrl = 'http://localhost:8000';   
    private baseUrl = 'https://foodiesplace1.herokuapp.com';
    constructor(private http: HttpClient) { }

    // getImages(id: number): Observable<Object> {
    //     return this.http.get(`${this.baseUrl}/${id}`);
    //   }
    
      getMealList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/meal/`);
      }
      getFoodList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/food/`);
      }
      getBevrageList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/bevrage/`);
      }
      getComboList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/combo/`);
      }

      getOneItem(id: number,field:string): Observable<Object> {
        return this.http.get(`${this.baseUrl}/${field}/${id}/`);
      }
      getSliderList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/slider/`);
      }
      getGalleryList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/gallery/`);
      }
      getGalleryVideoList(): Observable<any> {
        return this.http.get(`${this.baseUrl}/galleryvideo/`);
      }







    // getImages() {    
    //     return this.allImages = Imagesdelatils.slice(0);    
    // }    
    
    // getImage(id: number) {    
    //     return Imagesdelatils.slice(0).find(Images => Images.id == id)    
    // }    
}    

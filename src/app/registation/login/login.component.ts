import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


//////////////////

import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../_services/alert.service';
import { AuthenticationService } from '../_services/authentication.service';



import { SignupComponent } from '../../registation/signup/signup.component';

import {MatSnackBar} from '@angular/material/snack-bar';

//////////////////////////forget password//////////////////////
import { ForgetPasswordComponent } from '../../registation/forget-password/forget-password.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})



export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;





  constructor(public dialog: MatDialog,            
    public dialogRef: MatDialogRef<LoginComponent>,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private snackBar: MatSnackBar) {
          // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
          this.router.navigate(['/']);
      }
         }

         ngOnInit() {
          this.loginForm = this.formBuilder.group({
              username: ['', Validators.required],
              password: ['', Validators.required]
          });
  
          // get return url from route parameters or default to '/'
          this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
      }
  
      // convenience getter for easy access to form fields
      get f() { return this.loginForm.controls; }
  
      onSubmit() {
          this.submitted = true;
  
          // reset alerts on submit
          this.alertService.clear();
  
          // stop here if form is invalid
          if (this.loginForm.invalid) {
              return;
          }
  
          this.loading = true;
          this.authenticationService.login(this.f.username.value, this.f.password.value)
              .pipe(first())
              .subscribe(
                  data => {
                      this.router.navigate([this.returnUrl]);
                      this.dialogRef.close();
                      this.snackBar.open('you login sussesfully', 'Close',{
                        duration: 2000,
                      });
                  },
                  
                  error => {
                      this.alertService.error(error);
                      console.log('error');
                      // alert('Username or password is incorrect');
                      // return error('Username or password is incorrect');
                      this.loading = false;
                      this.snackBar.open('username or password is incorrect', 'Close',{
                        duration: 2000,
                      });
                  });
                  return Error('Username or password is incorrect');
      }
  




      openDialog(): void {
        const dialogRef = this.dialog.open(SignupComponent, {
          width: '350px',
          
        });
        this.dialogRef.close(LoginComponent); /////close when sign up open




        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          
        });
        

      }




      openpwdrstDialog(): void {
        const dialogRef = this.dialog.open(ForgetPasswordComponent, {
          width: '350px',
          
        });
        this.dialogRef.close(LoginComponent); /////close when sign up open




        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          
        });
        

      }







  onNoClick(): void {
    this.dialogRef.close();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { UserService } from '../../user.service';
import { User } from '../../user';
import { AuthenticationService } from '../../registation/_services/authentication.service';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {


  setpwdForm: FormGroup;

  currentUser: User;
  id: number;

  constructor(private authenticationService: AuthenticationService,private router: Router,private snackBar: MatSnackBar,private userService: UserService,private formBuilder: FormBuilder,) 
  {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
   }

  ngOnInit() {

    if(localStorage.getItem('currentUser')==null)
    {
      this.router.navigate(['home'])
    }

    // console.log(this.currentUser.id);
    // console.log(this.currentUser);



    this.setpwdForm = new FormGroup({
      
      old_password: new FormControl('', Validators.required),
      new_password: new FormControl('', Validators.required), 
      });
  }


  onSubmit() {

  this.userService.pwdChange(this.setpwdForm.value,this.currentUser.id)
  .subscribe(  
    data => {
      console.log(data);
      
      // this.uploadResponse = data;
      this.snackBar.open('password changed sussesfully.', 'close',{
        duration: 2000,
      });

    },
    error => {
      console.log(error)
     
      this.snackBar.open('password change  unsussesful.', 'close',{
        duration: 2000,
      });
    });
  }

}

import { Component, OnInit } from '@angular/core';

import {MatSnackBar} from '@angular/material/snack-bar';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
///////////////////

import { UserService } from '../../user.service';
import {CookieService} from 'ngx-cookie-service'

import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';

import { ReserconfirmComponent } from '../../reserconfirm/reserconfirm.component'



@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
  // const allCookies: {} = cookieService.getAll();
  // Cookies:any;
  fgtpwdForm: FormGroup;
  cookieValue ="";
  JSESSIONID;
  today: number = Date.now();
  constructor(public dialog: MatDialog, 
    public dialogRef: MatDialogRef<ForgetPasswordComponent>,public dialogRef1: MatDialogRef<ReserconfirmComponent, any>,
    private router: Router,private cookie:CookieService,private formBuilder: FormBuilder,private snackBar: MatSnackBar,private userService: UserService) { }

  ngOnInit() {
    // const allCookies: {} = cookieService.getAll();
    // this.cookie.set('csrftoken','vlhzDhPfU3CdfkX8i5EfQ8DlCCPpOkbCGzTNNol5gn863Vmz5ZMkEwC6Pvr2vHpp',this.today,'/','localhost',false,'Lax');
    // this.cookieValue = this.cookie.get('csrftoken');


    // this.cookie.set(
    //   name: string,
    //   value: string,
    //   expires?: number | Date,
    //   path?: string,
    //   domain?: string,
    //   secure?: boolean,
    //   sameSite?: 'Lax' | 'Strict'
    // )
    // this.cookieValue = this.cookie.get('csrftoken');
    // console.log( this.Cookies.getItem('csrftoken'))
    // this.Cookies.getItem('csrftoken')

    // if(Cookies.getItem('csrftoken')==null)
    // {
    //   // this.router.navigate(['home'])


    // console.log(this.Cookies);
          // this.Cookies.valid;
    // }

    this.fgtpwdForm = new FormGroup({
      // csrfmiddlewaretoken: new FormControl('arun'),
      email: new FormControl('', Validators.required),

  
    });
  }






  onSubmit() {

    this.save();
    // this.cookie.set('csrftoken','xxxxx',this.today,'/','localhost',false,'Lax');

  }


  save() {
    // updateForm.value


    
    this.userService.resetpwd(this.fgtpwdForm.value)
      .subscribe(  
        data => {
          console.log(data);
          // this.Cookies.valid;
          // this.uploadResponse = data;
          this.snackBar.open('Password reset e-mail has been sent.', 'close',{
            duration: 2000,
          });

          this.router.navigate(['/resetconfirm']);
        
          // )
          this.dialogRef.close();
          const dialogRef1 = this.dialog.open(ReserconfirmComponent, {
            width: '350px',
            
          });
        

          

        },
        error => {
          console.log(error)
         
          this.snackBar.open('Enter a valid Email address.', 'close',{
            duration: 2000,
          });

        }
        
        );
      
  }




}

import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { User } from '../../user';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


import { ActivatedRoute } from '@angular/router';

import {MatSnackBar} from '@angular/material/snack-bar';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


///////image upload////////////////mat diloge/////////
import { ImageuploadComponent } from '../../imageupload/imageupload.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
// var form = fileUploadForm;

// var data = new FormData(form);
// var path = "C:\\fakepath\\example.doc";
//  var filename = path.replace("arun", "");
//  console.log(filename);
// import * as $ from 'jquery';

// declare var $: any;

//  var _Profile_file = document.getElementById('FIP_file');

import * as $ from 'jquery';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  // onFileChanged(event) {
  //   this.file = event.target.files[0]
  //   var reader = new FileReader();
  //   reader.readAsDataURL(event.target.file[0]);
  //   reader.onload = (event) =>{
  //     this.selectedUser.file = (<FileReader>event.target).result;

  //   }
  // }
    

  // currentUser: User;
  disabled:true;
  selectedUser;
  id: number;



  updateForm: FormGroup;
  form: FormGroup;
  public uploader: FileUploader = new FileUploader({});
  file;
  uploadResponse = { status: '', message: '', filePath: '' };







  constructor(private sanitizer: DomSanitizer,
    public dialog: MatDialog, private formBuilder: FormBuilder,private snackBar: MatSnackBar,private userService: UserService,private router: Router,private route: ActivatedRoute) { }








  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map((fileItem) => {
      return fileItem.file;

    });
  }
//////////////////////////////////
  ngOnInit() {
    if(localStorage.getItem('currentUser')==null)
    {
      this.router.navigate(['home'])
    }



    this.route.data._isScalar.valueOf();
    
    this.file = this.getFiles();
      
      


///////////////////////////////send form//////////////////////////////////////updated form down
  //   this.updateForm = this.formBuilder.group({
  //     username: [ '', Validators.required],
  //     pin: ['', Validators.required],
  //     adress: ['', Validators.required],
  //     country: ['', Validators.required],
  //     city: ['', Validators.required],
  //     about: ['', Validators.required],

  //     // file: ['' , requiredFileType('png')],

  //     first_name: ['', Validators.required],

  //     last_name: ['', Validators.required],
  //     email: ['', [Validators.required, Validators.email]],
  //     password1: ['', [Validators.required, Validators.minLength(1)]],
  //     password2: ['', [Validators.required, Validators.minLength(1)]]
  // });


  this.updateForm = new FormGroup({
    username: new FormControl('', Validators.required),
    pin: new FormControl('', Validators.required),
    adress: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    // last: new FormControl('', Validators.required),
    about: new FormControl('', Validators.required),
    first_name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    password1: new FormControl('', Validators.required),
    password2: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    // file: new FormControl('',Validators.required),

  });
//////////////////////////////////////////////






  //////////////////////// get the user by id /////////////////////////


    const routeParams = this.route.snapshot.params;
    this.userService.getOneUser(routeParams.id).subscribe (
        data => {
          // console.log(data);

          this.selectedUser = data;
          // console.log(this.selectedUser)
        },
        error => console.log(error));




        
  }












//////////////////////open dialog box/////////////////////
  openDialog(): void {
    const dialogRef = this.dialog.open(ImageuploadComponent, {
      
      
      
    });
 
    dialogRef.afterClosed().subscribe(result => {
      console.log();
      
    });
    

  }





//////////////////////////save data/////////////////////////////

  save() {
    // updateForm.value


    
    this.userService.updateUser(this.updateForm.value,this.selectedUser.id)
      .subscribe(  
        data => {
          console.log(data);
          
          // this.uploadResponse = data;
          this.snackBar.open('Account sussesfully updated.', 'close',{
            duration: 2000,
          });

        },
        error => {
          console.log(error)
         
          this.snackBar.open('Account update unsussesful.', 'close',{
            duration: 2000,
          });

        }
        
        );
      
  }





  onSubmit() {

    this.save();

  }


  
  }





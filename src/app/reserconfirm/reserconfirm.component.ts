import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



@Component({
  selector: 'app-reserconfirm',
  templateUrl: './reserconfirm.component.html',
  styleUrls: ['./reserconfirm.component.scss']
})
export class ReserconfirmComponent implements OnInit {



  resetForm: FormGroup;
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<ReserconfirmComponent>,private snackBar: MatSnackBar,private formBuilder: FormBuilder,private userService: UserService) { }

  ngOnInit() {

    this.resetForm = new FormGroup({
      new_password1: new FormControl('', Validators.required),
      new_password2: new FormControl('', Validators.required),
      uid: new FormControl('', Validators.required),
      token: new FormControl('', Validators.required),
      
  
    });
  }




  save() {
    // updateForm.value


    
    this.userService.resetUser(this.resetForm.value)
      .subscribe(  
        data => {
          console.log(data);
          this.dialogRef.close();
          
          // this.uploadResponse = data;
          this.snackBar.open('Account sussesfully updated.', 'close',{
            duration: 2000,
          });

        },
        error => {
          console.log(error)
         
          this.snackBar.open('Account update unsussesful.', 'close',{
            duration: 2000,
          });

        }
        
        );
      
  }





  onSubmit() {

    this.save();

  }

}

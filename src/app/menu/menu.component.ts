import { Component,Inject, OnInit,Input } from '@angular/core';

import { ImageService } from '../image.service'; 
import { Image } from './../image';
import { Observable } from 'rxjs';



import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';




export interface DialogData {
  animal: string;
  name: string;
}




@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  animal: string;
  name: string;


  public searchText : string;
 public customerData : any;
 id:number;



 @Input() image: Image;

  images: Observable<Image[]>;
  


  errorMessage: string;
  interfacesBrief: Image[];
  interfacesBrief1: Image[];
  interfacesBrief2: Image[];
  interfacesBrief3: Image[];

 constructor(private imageService: ImageService,public dialog: MatDialog) { }

 
 ngOnInit() {





 this.customerData = [
 {"id":1,"name": 'Anil kumar', "Age": 34, "imgSrc" :'assets/card-1.gif'},
 {"id":2,"name": 'Sushil Singh', "Age": 24, "imgSrc" :'assets/card-1.gif'},
 {"id":3,"name": 'Aradhya Singh', "Age": 5, "imgSrc" :'assets/card-1.gif'},
 {"id":4,"name": 'Reena Singh', "Age": 28, "imgSrc" :'assets/card-1.gif'},
 {"id":5,"name": 'Alok Singh', "Age": 35, "imgSrc" :'assets/card-1.gif'},
 {"id":6,"name": 'Dilip Singh', "Age": 34, "imgSrc" :'assets/card-1.gif'},];
 


 this.getInterfaces();
 this.getInterfacesfood();
 this.getInterfacesbevrage();
 this.getInterfacescombo();


}

getInterfaces() {
  this.imageService.getMealList().subscribe(

    data => this.interfacesBrief = data,
    error => this.errorMessage = <any>error
  );
}

getInterfacesfood() {
  this.imageService.getFoodList().subscribe(

    data => this.interfacesBrief1 = data,
    error => this.errorMessage = <any>error
  );
}

getInterfacesbevrage() {
  this.imageService.getBevrageList().subscribe(

    data => this.interfacesBrief2 = data,
    error => this.errorMessage = <any>error
  );
}

getInterfacescombo() {
  this.imageService.getComboList().subscribe(

    data => this.interfacesBrief3 = data,
    error => this.errorMessage = <any>error
  );
}



openDialog(): void {
  const dialogRef = this.dialog.open(MenuExtend, {
    width: '600px', 
    
    data: {name: this.name, animal: this.animal}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    this.animal = result;
  });
}
    







}














@Component({
  selector: 'menu-extend',
  templateUrl: 'menu-extend.html',
   styleUrls: ['./menu.component.scss']
})

export class MenuExtend  {

  animal: string;
  name: string;
  displayedColumns = [ 'name', 'weight'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource1 = new MatTableDataSource(ELEMENT_DATA1);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource1.filter = filterValue.trim().toLowerCase();
  }
  constructor(public dialogRef:
    MatDialogRef<MenuExtend>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
      
    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }


  export interface PeriodicElement {
    name: string;
    
    weight: string;
    
    
  }

  
  
  const ELEMENT_DATA: PeriodicElement[] = [
    {name: 'Chocolate Milk', weight: '35/-'},
    { name: 'Iced tea brands', weight: '40/-'},
    { name: 'Milkshake', weight: '35/-'},
    { name: 'Beryllium', weight: '45/-'},
    { name: 'Frozen Drinks', weight: '50/-'},
    { name: 'Iced coffeen', weight: '45/-'},
    { name: 'Shamrock Shake', weight: '45/-'},
    { name: 'Lemon-lime drink', weight: '30/-'},
    { name: 'Juices', weight: '30/-'},
    { name: 'Sorbet tea', weight: '50/-'},
  ];
  export interface PeriodicElement1 {
   
    name1: string;
    
    weight1: string;
    
  }
  const ELEMENT_DATA1: PeriodicElement1[] = [

    { name1: 'Hot Chocolate', weight1: '40/-'},
    { name1: 'Coffee', weight1: '15/-'},
    { name1: 'Tea', weight1: '10/-'},
    { name1: 'Espresso', weight1: '20/-'},
    { name1: 'Butter tea', weight1: '20/-'},
    { name1: 'Cappuccino', weight1: '20/-'},
    { name1: 'indian filter coffe', weight1: '15/-'},
    { name1: 'Jigarthanda', weight1: '15/-'},
    { name1: 'ginger tea', weight1: '15/-'},
    

  ];












import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginComponent } from '../../registation/login/login.component';
import { AuthenticationService } from '../../registation/_services/authentication.service';
import { User } from '../../user';
import { Router } from '@angular/router';
import { UserService } from '../../user.service';
////////////////////change password/////////////////////
import { ChangePasswordComponent } from '../../registation/change-password/change-password.component';




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  // private baseUrl = 'http://localhost:8000/';
  currentUser: User;
  selectedUser:User;

  DJANGO_SERVER = 'http://127.0.0.1:8000';
  private id: any;
  imageURL:any;


  @Output() public sidenavToggle = new EventEmitter();


  










  constructor(private userService: UserService,public dialog: MatDialog,
              private router: Router,
              private authenticationService: AuthenticationService) {
              this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
              }


              ngOnInit() {
                // this.selectedUser
                // console.log(this.currentUser)             
                //  null
            
                // // const routeParams = this.route.snapshot.params;
                // this.userService.getOneUser(this.currentUser.id).subscribe (
                //     data => {
                //       // console.log(data);
                //       console.log(this.selectedUser)
                //     },
                //     error => console.log(error));

                // this.imageURL = `${this.DJANGO_SERVER}/${this.currentUser.file}`;
                // console.log(this.imageURL)
              }


              public onToggleSidenav = () => {
                this.sidenavToggle.emit();
              }



















  openDialog(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '350px',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

  changepassword(): void {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '350px',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }





  logout() 
  {
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }

}
































import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


import { ActivatedRoute } from '@angular/router';

//////////////////////////////////////

// import { UploadService } from '../upload.service';

import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { concat } from 'rxjs';

import {  mergeMap, catchError } from 'rxjs/operators';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

import { AuthenticationService } from '../registation/_services/authentication.service';

////////////////////////////////////////////////////////

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-imageupload',
  templateUrl: './imageupload.component.html',
  styleUrls: ['./imageupload.component.scss']
})












export class ImageuploadComponent implements OnInit {
//////////////////////////////////////////////////////////////////////////////
  DJANGO_SERVER = 'http://127.0.0.1:8000';


  public uploader: FileUploader = new FileUploader({});
  public hasBaseDropZoneOver: boolean = false;




  selectedUser:User;

  currentUser:User;

  formData: any;


  // DJANGO_SERVER = 'http://127.0.0.1:8000'
  form: FormGroup;
  response;
  imageURL;


















///////////////////////////private uploadService: UploadService///////////////


  constructor(public dialogRef: MatDialogRef<ImageuploadComponent>,private snackBar: MatSnackBar,private formBuilder: FormBuilder,private route: ActivatedRoute,private userService: UserService,private router: Router,private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
   }













  ngOnInit() {

    if(localStorage.getItem('currentUser')==null)
    {
      this.router.navigate(['home'])
    }

    console.log(localStorage.getItem('currentUser'));





    this.form = this.formBuilder.group({
      profile: [''],
      title:['']
    });
 
  }


  onChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('profile').setValue(file);
    }
  }

  onSubmit() {


    const formData = new FormData();
    formData.append('file', this.form.get('profile').value);
    console.log(formData.append('file', this.form.get('profile').value));

    // formData.append('username',localStorage.getItem('username'));

    formData.append('username',this.currentUser.username);
    // console.log(formData.append('username',localStorage.getItem('username')));
    formData.append('password1',this.currentUser.username);
    console.log(formData.append('password1',this.currentUser.username));
    formData.append('password2',this.currentUser.username);
    console.log(formData.append('password2',this.currentUser.username));
    formData.append('email',this.currentUser.email);
    console.log(formData.append('email',this.currentUser.email));



    formData.append('title',this.form.get('title').value);

    this.userService.upload(formData,this.currentUser.id).subscribe(
      (res) => {
        this.response = res;
        console.log(res);

        this.imageURL = `${res.file}`;
        console.log(res);
        console.log(this.imageURL);
        this.snackBar.open('Image upload sussesful.', 'close',{
          duration: 2000,
        });
        // this.dialogRef.close(
        //   {
        //     duration: 200000000,
        //   }
        // );


      },
      (err) => {  
        console.log(err);
       alert('error');
      }
    );
  }






































  // /////////////////////////////////////////////////////////////
  // fileOverBase(event): void {
  //   this.hasBaseDropZoneOver = event;
  // }


  // getFiles(): FileLikeObject[] {
  //   return this.uploader.queue.map((fileItem) => {
  //     return fileItem.file;

  //   });
  // }
  


  // upload() {


    
   
  //   let files = this.getFiles();
  //   console.log(files);
  //   let requests = [];
  //   files.forEach((file) => {
      

  //     let formData = new FormData();

  //     // formData.append('username', this.currentUser.username);


  //     formData.append('file' , file.rawFile, file.name);
  //     formData.append('',localStorage.getItem('currentUser'));






  //     console.log(formData);
  //     requests.push(this.userService.upload(this.formData,this.currentUser.id));
      
  //   });
 
  //   concat(...requests).subscribe(
  //     (res) => {
  //       console.log(res);
  //     },
  //     (err) => {  
  //       console.log(err);
  //     }
  //   );
  //   }

////////////////////////////////////////////////////////////////
  
}



















































































































































































































































































































// import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';
// import { User } from '../user';
// import { Observable } from 'rxjs';
// import { Router } from '@angular/router';


// import { ActivatedRoute } from '@angular/router';

// //////////////////////////////////////

// // import { UploadService } from '../upload.service';

// import { FileUploader, FileLikeObject } from 'ng2-file-upload';
// import { concat } from 'rxjs';

// import {  mergeMap, catchError } from 'rxjs/operators';
// import { Message } from '@angular/compiler/src/i18n/i18n_ast';

// import { AuthenticationService } from '../registation/_services/authentication.service';

// ////////////////////////////////////////////////////////

// import { FormBuilder, FormGroup, Validators } from '@angular/forms';


// @Component({
//   selector: 'app-imageupload',
//   templateUrl: './imageupload.component.html',
//   styleUrls: ['./imageupload.component.scss']
// })












// export class ImageuploadComponent implements OnInit {
// //////////////////////////////////////////////////////////////////////////////
//   DJANGO_SERVER = 'http://127.0.0.1:8000';


//   public uploader: FileUploader = new FileUploader({});
//   public hasBaseDropZoneOver: boolean = false;

// ///////////////////////////private uploadService: UploadService///////////////

  

//   constructor(private formBuilder: FormBuilder,private route: ActivatedRoute,private userService: UserService,private router: Router,private authenticationService: AuthenticationService) {
//     this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
//    }

//   selectedUser:User;

//   currentUser:User;

//   formData: any;
//   ngOnInit() {

//     if(localStorage.getItem('currentUser')==null)
//     {
//       this.router.navigate(['home'])
//     }

//     console.log(localStorage.getItem('currentUser'));
 
//   }












//   /////////////////////////////////////////////////////////////
//   fileOverBase(event): void {
//     this.hasBaseDropZoneOver = event;
//   }


//   getFiles(): FileLikeObject[] {
//     return this.uploader.queue.map((fileItem) => {
//       return fileItem.file;

//     });
//   }
  


//   upload() {


    
   
//     let files = this.getFiles();
//     console.log(files);
//     let requests = [];
//     files.forEach((file) => {
      

//       let formData = new FormData();

//       // formData.append('username', this.currentUser.username);


//       formData.append('file' , file.rawFile, file.name);
//       formData.append('',localStorage.getItem('currentUser'));






//       console.log(formData);
//       requests.push(this.userService.upload(this.formData,this.currentUser.id));
      
//     });
 
//     concat(...requests).subscribe(
//       (res) => {
//         console.log(res);
//       },
//       (err) => {  
//         console.log(err);
//       }
//     );
//     }

// ////////////////////////////////////////////////////////////////
  
// }

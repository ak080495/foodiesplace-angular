import { Component, OnInit, HostListener } from '@angular/core';
import { ConnectionService } from '../../connection.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';



import { User } from '../../user';
import { Messages } from '../../message';

export interface Food {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-contact',
 
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
  
})
export class ContactComponent implements OnInit {

  foods: Food[] = [
    
    {value: 'Feedback', viewValue: 'Feedback'},
    {value: 'Report a bug', viewValue: 'Report a bug'},
    {value: 'Feature request', viewValue: 'Feature request'}
  ];


contactForm: FormGroup;
disabledSubmitButton: boolean = true;
optionsSelect: Array<any>;


message: Messages = new Messages();

  @HostListener('input') oninput() {

    if (this.contactForm.valid) {
      this.disabledSubmitButton = false;
    }
  }

  constructor(private fb: FormBuilder, private connectionService: ConnectionService,private snackBar: MatSnackBar) {

  
  }

  ngOnInit() {
    this.contactForm = this.fb.group({
      'name': ['', Validators.required],
      'from_email': ['', Validators.compose([Validators.required, Validators.email])],
      'subject': ['', Validators.required],
      'message': ['', Validators.required],
      // 'contactFormCopy': [''],
      });

  this.optionsSelect = [
    { value: 'Feedback', label: 'Feedback' },
    { value: 'Report a bug', label: 'Report a bug' },
    { value: 'Feature request', label: 'Feature request' },
    // { value: 'Other stuff', label: 'Other stuff' },
    ];
  }


  save() {

  this.connectionService.createUser(this.message).subscribe(
    data => {
      this.snackBar.open('Your Message sent Successfully', 'close',{
        duration: 2000,
      });


      console.log(data);
    },
    error => {
      console.log(error)
    
      alert('Your Message not sent.');
    }

  ); 
  }

  onSubmit() {
    this.save();
  }
}




// this.connectionService.createUser(this.contactForm.value).subscribe(() => {
     
//   this.contactForm.reset();
//   this.disabledSubmitButton = true;
//   alert('Your message has been sent.');
// }, error => {
//   console.log('Error', error);
// });
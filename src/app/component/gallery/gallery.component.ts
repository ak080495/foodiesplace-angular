import { Component, OnInit,Input } from '@angular/core';
import { ImageService } from '../../image.service'; 
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';


interface gallery {
  title: string;
  image: string;

}

interface galleryvideo {
  Videoaddress: string;
 

}

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  public customerData : any;

  


  errorMessage: string;
  interfacesBrief: gallery[];

  interfacesBriefvideo: galleryvideo[];

  constructor(private imageService: ImageService,public sanitizer:DomSanitizer) { }

  ngOnInit() {
 
    
    this.getInterfaces();
    this.getVideoInterfaces();
  }

  getInterfaces() {
    this.imageService.getGalleryList().subscribe(
  
      data => this.interfacesBrief = data,
      error => this.errorMessage = <any>error
    );
  }

  getVideoInterfaces() {
    this.imageService.getGalleryVideoList().subscribe(
  
      data => this.interfacesBriefvideo = data,
      error => this.errorMessage = <any>error
    );
  }

}
